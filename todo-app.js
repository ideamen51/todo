(function() {
    function createAppTitle(title) {
        let appTitle = document.createElement('h2');
        appTitle.innerHTML = title;
        return appTitle;
    };

    function createTodoItemForm() {
        let form = document.createElement('form');
        let input = document.createElement('input');
        let buttonWrapper = document.createElement('div');
        let button = document.createElement('button');

        form.classList.add('input-group', 'mb-3');
        input.classList.add('form-control');
        input.id = 'input';
        input.placeholder = 'Введите значение нового дела';
        buttonWrapper.classList.add('input-group-append');
        button.classList.add('btn', 'btn-primary');
        button.textContent = 'Добавить дело';
        buttonWrapper.append(button);
        form.append(input);
        form.append(buttonWrapper);

        return {
            form,
            input,
            button,
        };
    };

    function createTodoList() {
        let list = document.createElement('ul');
        list.classList.add('list-group');
        return list;
    }

    
function createTodoItem(name) {
    let item = document.createElement('li');
    let buttonGroup = document.createElement('div');
    let doneButton = document.createElement('button');
    let deleteButton = document.createElement('button');

    item.classList.add('list-group-item', 'd-flex', 'justify-content-between', 'align-items-center');
    item.textContent = name;

    item.id = Math.random()*101,23;

    buttonGroup.classList.add('btn-group', 'btn-group-sm');
    doneButton.classList.add('btn', 'btn-success');
    doneButton.textContent = 'Готово';
    deleteButton.classList.add('btn', 'btn-danger');
    deleteButton.textContent = 'Удалить';

    buttonGroup.append(doneButton);
    buttonGroup.append(deleteButton);
    item.append(buttonGroup);

    return {
        item,
        doneButton,
        deleteButton,
    };
};

    function createTodoApp(container, title = 'Список дел', array, key) {

        let todoAppTitle = createAppTitle(title);
        let todoItemForm = createTodoItemForm();
        let todoList = createTodoList();
        

        container.append(todoAppTitle);
        container.append(todoItemForm.form);
        container.append(todoList);

        let arrayDefolt = [{name: 'Проснуться', done: true, id: todoItemForm.id }, {name: 'Покодить', done: false, id: todoItemForm.id}, {name: 'Покушать', done: false, id: todoItemForm.id}];
        function defaultArr() {
        for (let i = 0; i < arrayDefolt.length; i++) {
            let a = arrayDefolt[i];
            let b = a.name;
            let c = a.done;
            let formItem = createTodoItem(b);
            if (c === true) {
                formItem.item.classList.add('list-group-item-success')
            }else{
                formItem.item.classList.remove('list-group-item-success')
            };
            formItem.doneButton.addEventListener('click', function() {
                formItem.item.classList.toggle('list-group-item-success')
                });
                formItem.deleteButton.addEventListener('click', function() {
                    if (confirm('Вы уверены?')) {
                        formItem.item.remove();
                    }
                });
                todoList.append(formItem.item);
                };
        }; 
    defaultArr();

      
        function inputItem() {
            /*if (localStorage.getItem('пробник') != null)*/
            if (localStorage.getItem(key) != null) {
                for (let i = 0; i < array.length; i++) {
                    let a = array[i];
                    let b = a.name;
                    let c = a.done;
                    let formItem = createTodoItem(b);
                    if (c === true) {
                        formItem.item.classList.add('list-group-item-success')
                    } else {
                        formItem.item.classList.remove('list-group-item-success')
                    }; 
            

            formItem.doneButton.addEventListener('click', function() {
            formItem.item.classList.toggle('list-group-item-success')
            });
            formItem.deleteButton.addEventListener('click', function() {
                if (confirm('Вы уверены?')) {
                    formItem.item.remove();
                }
            });
            todoList.append(formItem.item);
            };
        };
         };
         inputItem();
     
        todoItemForm.button.disabled = true;

        function buttonDisabled() {
            if (todoItemForm.input.value) {
                todoItemForm.button.disabled = false;
            }else if(!todoItemForm.input.value) {
                todoItemForm.button.disabled = true;
            }
        }

        let arrayJson = [];
        let objJson = {};
        
        todoItemForm.input.addEventListener('input', buttonDisabled);
        todoItemForm.form.addEventListener('submit', function(e) {
            e.preventDefault();
            if (!todoItemForm.input.value) {
                return;
            }
            let todoItem = createTodoItem(todoItemForm.input.value);
            objJson.name = todoItemForm.input.value;
            objJson.done = false;
            objJson.id = todoItem.item.id;

    

            todoItem.doneButton.addEventListener('click', function() {
                let objJsons = JSON.parse(localStorage.getItem(key));
                todoItem.item.classList.toggle('list-group-item-success');
                let arrayJson =  objJsons.map(obj => {
                        if (obj.id === todoItem.item.id && obj.done === false) {
                            obj.done = true;
                        }else{
                            obj.done = false;
                        }; 
                        return obj; 
                    });
                localStorage.setItem(key, JSON.stringify(arrayJson));

            });
            todoItem.deleteButton.addEventListener('click', function() {
                if (confirm('Вы уверены?')) {
                    let objJsons = JSON.parse(localStorage.getItem(key));
                    let arrayJsonNew =  objJsons.filter(obj => obj.id != todoItem.item.id);
                    localStorage.setItem(key, JSON.stringify(arrayJsonNew));
                    todoItem.item.remove();
                };
        });
            todoList.append(todoItem.item);
            todoItemForm.input.value = '';
            todoItemForm.button.disabled = true;

            let getArray = localStorage.getItem(key);
            if (getArray) {
                let getArrayParse = JSON.parse(getArray);
                getArrayParse.push(objJson);
                localStorage.setItem(key, JSON.stringify(getArrayParse));
            }else{
                localStorage.setItem(key, JSON.stringify([objJson]));
            }
            let getArrayParse = JSON.parse(getArray);
        }); 
    };
    window.createTodoApp = createTodoApp;
})();

/* для разделения дел для мамы папы, использовать параметр стор для присвоения его
*/
